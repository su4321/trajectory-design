from propagator import Prop as OP
from propagator import null_perts
from propagator import null_propulsion
from propagator import planet
import numpy as np
import tools


#time parameters 
tspan = 5*60*60*24
#tspan = 60*60*24/10
dt=100.


if __name__ == '__main__':

	perts = null_perts()
	perts['J2'] = False
	perts['SRP'] = False
	perts['CR'] = 0.1
	perts['A_SRP'] =  0.01
	propulsion = null_propulsion()
	propulsion['Low-thrust'] = True
	propulsion['thrust_direction'] = 'v'
	propulsion['sign'] = 1
	propulsion['thrust'] = 0.0215
	propulsion['isp'] = 800.
	planet = planet()

	r = planet['Radius'] + 500
	v = np.sqrt(planet['Mu']/r)

	mass0 = 15.
	state = np.array([r,0,0,0,v,0])

	op=OP(state,tspan,dt,deg=True,coes=False,perts=perts,mass0=mass0,propulsion=propulsion,solver='lsoda')
	op.plot_3d(show_plot=True,save_plot=False)

