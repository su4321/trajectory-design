import numpy as np 
import matplotlib.pyplot as plt
from scipy.integrate import ode
import tools as tools
from mpl_toolkits.mplot3d import Axes3D


def null_perts(): 
	return{
	'J2':False, 
	'SRP': False,
	'A_SRP': False,
	'CR': False,
	'aero': False
	}

def null_propulsion():
	return{
	'Low-thrust' : False,
	'Impulsive' : False,
	'thrust_direction ' : 'v',
	'thrust_sign' : 1,
	'thrust' : 0.,
	'isp' : 2000.
	}

def planet():
	return{
	'Name':'Earth',
	'Radius': 6371.,
	'Mu' : 3.986004418e5,
	'J2' : 1.082635854e-3,
	'color': 'Greens'
	}

class Prop:
	def __init__(self,state0,tspan,dt,coes=False,deg=True,perts=null_perts(),
		propulsion = null_propulsion(),mass0=2000.,solver = 'DOP853', planet=planet()):
		if coes:
			print('Not implemented yet')
		
		else:
			self.r0 = state0[:3]
			self.v0 = state0[3:]

		self.tspan = tspan
		self.dt = dt
		self.n_steps = int(np.ceil(self.tspan/self.dt))+1
		self.mass0 = mass0
		self.ys = np.zeros((self.n_steps,7))
		self.ts = np.zeros((self.n_steps,1))
		self.y0 = self.r0.tolist()+self.v0.tolist()+[self.mass0]


		self.ts[0]=0
		self.ys[0]=self.y0

		self.step=1

		# initiate solver
		self.solver=ode(self.diffy_q)
		self.solver.set_integrator(solver)
		self.solver.set_initial_value(self.y0,t=0.0)


		#define perturbations dictionary
		self.perts=perts
		self.propulsion = propulsion
		self.planet = planet


		self.propagate_orbit()


	def propagate_orbit(self):
		print('Propagating orbit..') 
		#propagate orbit
		while self.solver.successful() and self.step<self.n_steps:
			self.solver.integrate(self.solver.t+self.dt)
			self.ts[self.step]=self.solver.t
			self.ys[self.step]=self.solver.y
			self.step+=1
		self.rs=self.ys[:,:3]
		self.vs=self.ys[:,3:6]
		self.masses=self.ys[:self.step,-1]
		self.alts=(np.linalg.norm(self.rs,axis=1)-self.planet['Radius']).reshape((self.step,1))


	def diffy_q(self,t,y):
		#unpack state
		rx,ry,rz,vx,vy,vz,mass=y
		r=np.array([rx,ry,rz])
		v=np.array([vx,vy,vz])

		#norm of the radius vector
		norm_r = np.linalg.norm(r)


		#two body acceleration
		a=-r*self.planet['Mu']/norm_r**3
		#ax,ay,az=-r*self.cb['mu']/norm_r**3
		dmdt=0
		# J2 perturbation
		if self.perts['J2']:
			z2 = r[2]**2
			r2 = norm_r**2
			tx = r[0]/norm_r*(5*z2/r2-1)
			ty = r[1]/norm_r*(5*z2/r2-1)
			tz = r[2]/norm_r*(5*z2/r2-3)

			a_j2 = 1.5*self.cb['J2']*self.planet['Mu']*self.planet['Radius']**2/norm_r**4*np.array([tx,ty,tz])
			a+= a_j2
            
        #aerodynamic drag
		if self.perts['aero']:
		#calculate altitude and air density
			print('Drag not implemented yet')
			a+=0
        
		if self.propulsion['Low-thrust']:
			if self.propulsion['thrust_direction'] == 'v':

				a+=self.propulsion['thrust_sign']*tools.unitary(v)*self.propulsion['thrust']/self.mass0 /1000. #km/s**2 check

			if self.propulsion['thrust_direction'] == 'n':

				a+= self.propulsion['thrust_sign']*self.propulsion['thrust']/self.mass0 /1000.*tools.unitary(np.cross(r,v))

			if self.propulsion['thrust_direction'] == 'b':

				a+= self.propulsion['thrust_sign']*self.propulsion['thrust']/self.mass0 /1000.*tools.unitary(r)

			#derivative of total mass
			dmdt=-self.propulsion['thrust']/self.propulsion['isp']/9.81



		if self.perts['SRP']:
			c = 299792458. #[m/s]
			sigma = 5.67e-8 #[W/m2/K4]
			T_eff_sun = 5780. #[K]
			R_sun = 696340. #[km]
			Ps = sigma * T_eff_sun*T_eff_sun*T_eff_sun*T_eff_sun*(R_sun/norm_r)*(R_sun/norm_r) #[W/m2]
			Fs = Ps/c #[N/m^2]
	

			a_SRP = -(1.+self.perts['CR'])*Fs*self.perts['A_SRP']/mass*r/norm_r / 1000.   #to be in km  #pannel aligment 
			a+= a_SRP

		return [vx,vy,vz,a[0],a[1],a[2],dmdt]
 

	def plot_3d(self,show_plot=False,save_plot=False):
		fig=plt.figure(figsize=(6,6))
		ax=fig.add_subplot(111,projection='3d')

		#plot central body
		_u,_v=np.mgrid[0:2*np.pi:20j,0:np.pi:10j]
		_x=self.planet['Radius']*np.cos(_u)*np.sin(_v)
		_y=self.planet['Radius']*np.sin(_u)*np.sin(_v)
		_z=self.planet['Radius']*np.cos(_v)
		surfcolor= self.planet['color']
		ax.plot_surface(_x,_y,_z,cmap=surfcolor,rstride=1, cstride=1)

		#scatter
		ax.plot(self.rs[:,0],self.rs[:,1],self.rs[:,2],label='Trajectory')
		ax.plot([self.rs[0,0]],[self.rs[0,1]],[self.rs[0,2]],'ko',label='Initial Position')

		max_val=np.max(np.abs(self.rs))
		ax.set_xlim([-max_val,max_val])
		ax.set_ylim([-max_val,max_val])
		ax.set_zlim([-max_val,max_val])
		ax.set_xlabel('X [km]'); ax.set_ylabel('Y [km]'); ax.set_zlabel('Z [km]');
		#ax.set_aspect('equal')
		title='3d orbit representation in '+self.planet['Name']+' J2000 reference frame'
		ax.set_title(title,pad=30)

		plt.legend()
		if save_plot:
			plt.savefig('3dplot.png',dpi=300)
		if show_plot:
			plt.show()