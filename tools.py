import numpy as np 
import spiceypy as spiceypy

def unitary(v):
	u = v/np.linalg.norm(v)
	return u

def  coes2rv(coes,mu=mu_earth, degrees = True, spice = False):
	# Returns the state vector of a set of keplerian elements
	# INPUTS:
	# - coes(list) - Keplerian elements in the following order [a,e,i,ta,aop,raan] (SMA in km and angles in radians or degrees depending in degrees variable)
	# - mu (int) - Gravitational parameter of the orbiting body. By default the Earth. Units are km^3/s^2
	# - degrees (boolean) - Units of angles. By default degrees.
	# - spice (boolean) - If True, computations performed by NASA's SPICE routines. If False, computations performed by own routines

	if spice:
		a, e, i, ta, aop, raan = coes
		if degrees:
			i    *= d2r
			ta   *= d2r
			aop  *= d2r
			raan *= d2r

		rp = a * ( 1 - e )

		return spiceypy.conics( [ rp, e, i, raan, aop, ta, 0, mu], 0 )


	else:

		a,e,inc,ta,aop,raan=coes

		if degrees: 
			inc *= d2r
			ta*=d2r
			aop*=d2r
			raan*=d2r
		h = sqrt(a*mu*(1-e*e))
		p = h*h/mu
		u = aop + ta
		r = p/(1.+e*cos(ta))
		rdot = mu*e*sin(ta)/h 

		x = r*(cos(u)*cos(raan) - sin(u)*cos(inc)*sin(raan))
		y = r*(cos(u)*sin(raan) + sin(u)*cos(inc)*cos(raan))
		z = r*(sin(u)*sin(inc))

		v_x = rdot*x/r - (sin(u)*cos(raan) + cos(u)*cos(inc)*sin(raan))*h/r
		v_y = rdot*y/r + (-sin(u)*sin(raan) + cos(u)*cos(inc)*cos(raan))*h/r
		v_z = rdot*z/r + (cos(u)*sin(inc))*h/r

		return [x,y,z,v_x,v_y,v_z]


def rv2coes(state, mu=mu_earth, degrees = True, spice = False, et=0):	
	# Returns the set of keplerian elements of a given state vector
	# INPUTS:
	# - state(list) - State vector (position and velocity) of a spacecraft expresed in km
	# - mu (int) - Gravitational parameter of the orbiting body. By default the Earth. Units are km^3/s^2
	# - degrees (boolean) - Units of angles. By default degrees.
	# - spice (boolean) - If True, computations performed by NASA's SPICE routines. If False, computations performed by own routines
	# - et (int) - Epoch

	x,y,z,v_x,v_y,v_z = state

	r = np.array([x,y,z])
	v = np.array([v_x,v_y, v_z])

	if spice:
		rp,e,inc,raan,aop,ma,t0,mu,ta,a,T = spiceypy.oscltx( state, et, mu)
		if degrees: 
			inc *= r2d
			ta *= r2d
			aop *= r2d
			raan *= r2d
		return [a,e,inc,ta,aop,raan]



	else:

		r_norm=np.linalg.norm(r)
		h=np.cross(r,v)
		h_norm=np.linalg.norm(h)
		inc=m.acos(h[2]/h_norm)
		

		e=((np.linalg.norm(v)**2-mu/r_norm)*r-np.dot(r,v)*v)/mu
		e_norm=np.linalg.norm(e)
		N=np.cross([0,0,1],h)
		N_norm = np.linalg.norm(N)
		raan=m.acos(N[0]/N_norm)
		if N[1]<0: raan=2*np.pi-raan
		aop=m.acos(np.dot(N,e)/N_norm/e_norm)
		if e[2]<0: aop=2*np.pi-aop
		calculo =np.dot(e,r)/e_norm/r_norm
		if calculo <0: calculo = np.abs(calculo)
		if calculo >1: calculo = 1
		ta = m.acos(calculo) 
		if np.dot(r,v)<0: ta=2*np.pi-ta
		a = r_norm*(1+e_norm*m.cos(ta))/(1-e_norm**2)

		if degrees: return[a,e_norm,inc*r2d,ta*r2d,aop*r2d,raan*r2d]
		else: return [a,e_norm,inc,ta,aop,raan]